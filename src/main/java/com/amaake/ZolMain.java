package com.amaake;

import com.amaake.Model.Zolurl;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.amaake.Model._MappingKit;
import org.apache.log4j.Logger;
import us.codecraft.webmagic.Spider;

import javax.management.JMException;
import java.util.ArrayList;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/10/12 0012
 * \* Time: 3:12
 * \* Description: 春眠不觉晓，起来敲代码。
 * \ 主类，爬虫入口
 */
public class ZolMain {
    private static Logger log = Logger.getLogger(ZolMain.class);

    public static void main(String[] args) throws JMException {
        log.info("-------------设置数据库");
        DruidPlugin dp = new DruidPlugin("jdbc:mysql://127.0.0.1:3306/zolpc?characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull", "root", "root");
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        _MappingKit.mapping(arp);
        dp.start();
        arp.start();
        ZolMain zol = new ZolMain();

        //zol.pcljstatic();
        zol.pcstart();

    }

    //爬虫爬取数据
    public void pcstart(){
        log.info("-------------爬虫开始爬取数据");
        List<Zolurl> zolurlList = Zolurl.dao.find("select * from zolurl");
        List<String> urllist = new ArrayList<String>();
        int i = 0;
        for(Zolurl url : zolurlList){
            String urlst = url.getUrl();
            if(urlst != null && !urlst.equals("")){
                urllist.add(urlst);
            }
        }
        Spider.create(new ZolpcPageProcesser())
                .addUrl((String[])urllist.toArray(new String[urllist.size()]))
                .thread(5).run();
    }

    //更新爬虫连接
    public void pcljstatic() throws JMException {
        log.info("-------------更新爬虫连接");
        //主板1
        Spider.create(new ZolupurlPageProcesser(1)).addUrl("http://detail.zol.com.cn/motherboard/").thread(5).run();

        //显卡2
        Spider.create(new ZolupurlPageProcesser(2)).addUrl("http://detail.zol.com.cn/vga/").thread(5).run();

        //CPU3
        Spider.create(new ZolupurlPageProcesser(3)).addUrl("http://detail.zol.com.cn/cpu/").thread(5).run();

        //内存4
        Spider.create(new ZolupurlPageProcesser(4)).addUrl("http://detail.zol.com.cn/memory/").thread(5).run();

        //硬盘5
        Spider.create(new ZolupurlPageProcesser(5)).addUrl("http://detail.zol.com.cn/hard_drives/").thread(5).run();

        //机箱6
        Spider.create(new ZolupurlPageProcesser(6)).addUrl("http://detail.zol.com.cn/case/").thread(5).run();

        //电源7
        Spider.create(new ZolupurlPageProcesser(7)).addUrl("http://detail.zol.com.cn/power/").thread(5).run();

        //散热器8
        Spider.create(new ZolupurlPageProcesser(8)).addUrl("http://detail.zol.com.cn/cooling_product/").thread(5).run();

        //固态硬盘9
        Spider.create(new ZolupurlPageProcesser(9)).addUrl("http://detail.zol.com.cn/solid_state_drive/").thread(5).run();

        //光驱10
        Spider.create(new ZolupurlPageProcesser(10)).addUrl("http://detail.zol.com.cn/dvdrw/").thread(5).run();

        //液晶显示器11
        Spider.create(new ZolupurlPageProcesser(11)).addUrl("http://detail.zol.com.cn/lcd/").thread(5).run();
    }
}