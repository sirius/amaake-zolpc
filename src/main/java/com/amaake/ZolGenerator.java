package com.amaake;

import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/10/12 0012
 * \* Time: 3:23
 * \* Description: 春眠不觉晓，起来敲代码。
 * \数据库自动生成model
 */
public class ZolGenerator {


    public static void main(String[] args){
        // base model 所使用的包名
        String baseModelPackageName = "com.amaake.Model.base";
        // base model 文件保存路径
        String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/amaake/Model/base";
        System.out.println(baseModelOutputDir);
        // model 所使用的包名 (MappingKit 默认使用的包名)
        String modelPackageName = "com.amaake.Model";
        // model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
        String modelOutputDir = baseModelOutputDir + "/..";
        DruidPlugin dp = new DruidPlugin("jdbc:mysql://127.0.0.1:3306/zolpc?characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull", "root", "root");
        dp.start();
        Generator gernerator = new Generator(dp.getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
        // 设置数据库方言
        gernerator.setDialect(new MysqlDialect());
        // 设置是否在 Model 中生成 dao 对象
        gernerator.setGenerateDaoInModel(true);
        // 设置是否生成字典文件
        gernerator.setGenerateDataDictionary(false);
        // 生成
        gernerator.generate();
    }
}